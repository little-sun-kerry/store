export default [
  {
    name: "主页",
    path: "/Home",
    redirect: "/Home/One",
    meta: { isShow: true, isTrue: true },
    component: () => import("@/pages/home"),
    children: [
      {
        name: "第一页",
        path: "One",
        meta: { isShow: true, isTrue: true },
        component: () => import("@/pages/home/One"),
      },
      {
        name: "第二页",
        path: "Two",
        meta: { isShow: true, isTrue: true },
        component: () => import("@/pages/home/Two"),
      },
      {
        name: "第三页",
        path: "Three",
        meta: { isShow: true, isTrue: true },
        component: () => import("@/pages/home/Three"),
      },
      {
        name: "第四页",
        path: "Four",
        meta: { isShow: true, isTrue: true },
        component: () => import("@/pages/home/Four"),
      },
    ],
  },
  {
    name: "登录",
    path: "/Login",
    component: () => import("@/pages/Login"),
    meta: { isShow: false, isTrue: false },
  },
  {
    name: "注册",
    path: "/Register",
    component: () => import("@/pages/Register"),
    meta: { isShow: false, isTrue: false },
  },
  {
    name: "个人中心",
    path: "/Individual",
    redirect: "/Individual/MyOrder",
    component: () => import("@/pages/individual"),
    meta: { isShow: true, isTrue: false },
    children: [
      {
        name: "我的订单",
        path: "MyOrder",
        component: () => import("@/pages/individual/myOrder"),
        meta: { isShow: true, isTrue: false },
      },
      {
        name: "退款售后",
        path: "AfterRefund",
        component: () => import("@/pages/individual/afterRefund"),
        meta: { isShow: true, isTrue: false },
      },
      {
        name: "优惠券",
        path: "Coupon",
        component: () => import("@/pages/individual/coupon"),
        meta: { isShow: true, isTrue: false },
      },
      {
        name: "地址管理",
        path: "MyAddress",
        component: () => import("@/pages/individual/myAddress"),
        meta: { isShow: true, isTrue: false },
      },
    ],
  },
  {
    name: "商城",
    path: "/Mall",
    component: () => import("@/pages/Mall"),
    meta: { isShow: true, isTrue: true },
  },
  {
    name: "商品信息",
    path: "/shopInfo",
    component: () => import("@/pages/shopInfo"),
    meta: { isShow: true, isTrue: false },
  },
  {
    name: "购物车",
    path: "/shopcart",
    component: () => import("@/pages/shopcart"),
    meta: { isShow: true, isTrue: false },
  },
  {
    name: "订单",
    path: "/Order",
    component: () => import("@/pages/Order"),
    meta: { isShow: true, isTrue: false },
    beforeEnter: (to, from, next) => {
      if (from.path == "/shopcart" || from.path == "/") {
        next();
      } else {
        next(false);
      }
    },
  },
  {
    name: "订单信息",
    path: "/OrderInfo",
    component: () => import("@/pages/orderInfo"),
    meta: { isShow: true, isTrue: false },
    beforeEnter: (to, from, next) => {
      if (
        from.path == "/Order" ||
        from.path == "/" ||
        from.path == "/Individual"
      ) {
        next();
      } else {
        next(false);
      }
    },
  },
  {
    path: "*",
    redirect: "/Home",
  },
];
