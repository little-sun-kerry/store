import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./route";

Vue.use(VueRouter);

let originPush = VueRouter.prototype.push;
let originReplace = VueRouter.prototype.replace;

VueRouter.prototype.push = function (location, resolve, reject) {
  if (resolve && reject) {
    originPush.call(this, location, resolve, reject);
  } else {
    originPush.call(
      this,
      location,
      () => {},
      () => {}
    );
  }
};
VueRouter.prototype.replace = function (location, resolve, reject) {
  if (resolve && reject) {
    originReplace.call(this, location, resolve, reject);
  } else {
    originReplace.call(
      this,
      location,
      () => {},
      () => {}
    );
  }
};

let router = new VueRouter({
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (to.path.indexOf("Login") !== -1 || to.path.indexOf("Register") !== -1) {
      return { y: 200 };
    }
    if (to.path.indexOf("shopInfo") !== -1) {
      return { y: 0 };
    }
  },
});

router.beforeEach(async (to, from, next) => {
  let Token=JSON.parse(localStorage.getItem('Token'));
  if(Token){
    if(to.path=='/Login'){
      next('/Home')
    }else{
      next()
    }
  }else{
    let toPath=to.path
    if(toPath.indexOf('shop')!=-1||toPath.indexOf('Order')!=-1){
      next('/Login')
    }else{
      next()
    }
  }
});

export default router;
