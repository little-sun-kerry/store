import Mock from "mockjs";
import banner from "./banner.JSON";
import Right from "./right.JSON";
import Advertising from "./advertising.JSON";
import router from "@/router";
// import {shopCart} from '@/mock/shopCart'
import shopCartInfo from "./shopCart.JSON";

Mock.mock("/mock/banner", { code: 200, data: banner });
Mock.mock("/mock/right", { code: 200, data: Right });
Mock.mock("/mock/advertising", { code: 200, data: Advertising });

const { userId } = Mock.mock({
  "userId|1-100": 1,
});

const { myId } = Mock.mock({
  "myId|1-100": 1,
});

let cartInfo = [];

const { orderId } = Mock.mock({
  "orderId|100-100000": 1,
});

let shopCartInfos=[]

// 登录规则
export const getLoginQuery = (url) => {
  const myIndex = url.indexOf("?");
  const myQuery = url.substr(myIndex + 1);
  const myItems = myQuery.split("&");
  let arr = [];
  myItems.forEach((item) => {
    arr = arr.concat(item.split("=")[1]);
  });
  let myUser = JSON.parse(localStorage.getItem("username"));
  let myWord = JSON.parse(localStorage.getItem("password"));
  if (
    localStorage.getItem("username") !== null &&
    localStorage.getItem("password") !== null
  ) {
    if (myUser === arr[0] && myWord === arr[1]) {
      alert("恭喜，登录成功");
      router.push({
        path: "/Home",
        query: {
          userId: userId,
        },
      });
    } else {
      alert("账号或密码书写错误！！！");
    }
  } else {
    router.push("/Register");
  }
};

// 注册规则
export const getRegisterQuery = (url) => {
  const myIndex = url.indexOf("?");
  // console.log(myIndex);
  const myQuery = url.substr(myIndex + 1);
  // console.log(myQuery);
  const myItems = myQuery.split("&");
  // console.log(myItems);
  let arr = [];
  myItems.forEach((item) => {
    arr = item.split("=");
    localStorage.setItem(`${arr[0]}`, JSON.stringify(arr[1]));
  });
  router.push("/Login");
};

// 退出规则
export const getLogoutQuery = (url) => {
  const myIndex = url.indexOf("?");
  const myQuery = url.substr(myIndex + 1);
  const myItems = myQuery.split("&");
  let arr = [];
  myItems.forEach((item) => {
    arr = item.split("=");
    console.log(arr[0]);
    localStorage.removeItem(`${arr[0]}`);
  });
  router.push({
    path: "/Home",
    query: {
      userId: myId,
    },
  });
};

// 获取商品详情
export const getCartInfo = (url) => {
  const myIndex = url.indexOf("?");
  const myQuery = url.substr(myIndex + 1);
  let arr = myQuery.split("=");
  let info = JSON.parse(arr[1]);
  cartInfo = cartInfo.concat(info);
};

// 删除商品信息
export const deleteData = (url) => {
  const myIndex = url.indexOf("?");
  const myQuery = url.substr(myIndex + 1);
  cartInfo = cartInfo.filter((item) => item.id != myQuery);
  console.log(cartInfo);
};

// 搜索商品信息
export const searchData = (url) => {
  const myIndex = url.indexOf("?");
  const myQuery = url.substr(myIndex + 1);
  if(myQuery!==''){
    shopCartInfos=shopCartInfo.filter(item=>{
      return item.text.indexOf(myQuery)!=-1
    })
  }else{
    shopCartInfos=shopCartInfo
  }
};

//拦截登录请求
Mock.mock(/\/mock\/Login/, "get", (options) => {
  console.log(options);
  getLoginQuery(options.url);
});

// 拦截注册请求
Mock.mock(/\/mock\/Register/, "get", (options) => {
  console.log(options);
  getRegisterQuery(options.url);
});

// 拦截退出请求
Mock.mock(/\/mock\/Logout/, "get", (options) => {
  console.log(options);
  getLogoutQuery(options.url);
});

// 拦截商品信息请求
Mock.mock(/\/mock\/shopCartInfo/, "get", (options) => {
  searchData(options.url)
  return {
    status: 200,
    message: "获取成功",
    shopCartInfo:shopCartInfos,
  };
});

// 拦截购物车信息
Mock.mock(/\/mock\/shopcart/, "get", (options) => {
  getCartInfo(options.url);
  return {
    code: 200,
    data: cartInfo,
  };
});

// 拦截商品删除信息
Mock.mock(/\/mock\/delete/, "get", (options) => {
  deleteData(options.url);
});

// 拦截订单号信息
Mock.mock("/mock/PayInfo", "get", () => {
  return {
    orderId: 143,
  };
});

// 拦截搜索请求
Mock.mock(/\/mock\/search/, "get", (options) => {
  searchData(options.url);
  return {
    shopCartInfos
  }
});
