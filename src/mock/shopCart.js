import Mock from "mockjs";

export const getNum=()=>{
    return Math.ceil(Math.random()*10)
}

export const {shopCartInfo} = Mock.mock({
    "shopCartInfo|20":[
        {
            "id|0-10000":1,
            "imgUrl|+1":[
                './image/advertising1.png',
                './image/advertising2.png',
                './image/advertising3.png',
                './image/advertising4.png',
                './image/advertising5.png',
                './image/advertising6.png',
                './image/advertising7.png',
                './image/advertising8.png',
                './image/advertising9.png',
                './image/advertising10.png'
            ],
            text:'【原神】原神主题系列鼠标垫 Genshin',
            "price|50-5000":50,
        }
    ]
})

export const shopCart=()=>{
    return shopCartInfo;
}