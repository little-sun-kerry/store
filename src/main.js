import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import elementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import "swiper/swiper-bundle.css";
import * as API from '@/api'
import "@/mock/mockServer";
import Carousel from "@/components/carousel";
import '@/font/iconfont.js'
import "@/font/iconfont.css"
import store from '@/store'

Vue.config.productionTip = false
Vue.use(elementUI)
Vue.component(Carousel.name, Carousel);

new Vue({
  render: h => h(App),
  beforeCreate(){
    Vue.prototype.$bus=this
    Vue.prototype.$API=API
  },
  router,
  store
}).$mount('#app')
