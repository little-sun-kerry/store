import axios from "axios";
import nprogress from "nprogress";
import "nprogress/nprogress.css";

const requestUser = axios.create({
  baseURL: "/user",
  timeout: 5000,
});

requestUser.interceptors.request.use((config) => {
  config.headers["Content-Type"] = "application/x-www-form-urlencoded";
  if(localStorage.getItem('userToken')){
    config.headers.usertoken=JSON.parse(localStorage.getItem('userToken'))
  }
  if(localStorage.getItem('Token')){
    config.headers.Authorization=JSON.parse(localStorage.getItem('Token'))
  }
  nprogress.start();
  return config;
});

requestUser.interceptors.response.use(
  (res) => {
    nprogress.done();
    return res.data;
  },
  (error) => {
    return Promise.reject(new Error("faile"));
  }
);

export default requestUser;
