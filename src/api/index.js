import mockAjax from "./mockAjax";
import request from "./request";
import requestUser from "./users";

// 请求banner.JSOND的数据
export const mockBannerData = () => mockAjax.get("/banner");

// 请求right.JSOND的数据
export const mockRightData = () => mockAjax.get("/right");

// 请求advertising.JSOND的数据
export const mockAdvertisingData = () => mockAjax.get("/advertising");

// 请求登录
export const reqLoginInfo = (username, password) =>
  requestUser({ url: "/Login", data: { username, password }, method: "post" });

// 请求注册
export const reqRegUser = (username, password, confirmPassword) =>
  requestUser({
    url: "/regUser",
    data: {
      username,
      password,
      confirmPassword,
    },
    method: "post",
  });

//请求用户信息
export const reqGetUserInfo = () =>
  requestUser({
    url: "/userinfo",
    method: "get",
  });

// 请求退出登录
export const reqLogOut = () =>
  requestUser({
    url: "Logout",
    method: "get",
  });

// 请求商品信息
export const mockShopCartInfo = (keyWrad) =>
  mockAjax.get(`/shopCartInfo?${keyWrad}`);

//存储购物车消息
export const reqSaveCartInfo = (info) =>
  requestUser({
    url: "/SaveCartInfo",
    data: {
      info,
    },
    method: "post",
  });

// 请求购物车信息
export const reqGetCartInfo = () =>
  requestUser({
    url: "/GetCartInfo",
    method: "get",
  });

//更新购物车信息
export const reqUpdateCartInfo = (info) =>
  requestUser({
    url: "/updateCartInfo",
    data: {
      info,
    },
    method: "post",
  });

//删除购物车信息
export const reqDeleteCartInfo = (info) =>
  requestUser({
    url: "/DeleteCartInfo",
    data: {
      info,
    },
    method: "post",
  });

//保存用户地址信息
export const reqSaveAddressInfo = (info) =>
  requestUser({
    url: "/saveAddressInfo",
    data: {
      info,
    },
    method: "post",
  });

//请求用户地址信息
export const reqGetAddressInfo = () =>
  requestUser({
    url: "/getAddressInfo",
    method: "get",
  });

//请求修改默认地址信息
export const reqChangeDefaultAddress = (id) =>
  requestUser({
    url: "/changeDefaultAddress",
    data: {
      id,
    },
    method: "post",
  });

//删除地址信息
export const reqDeleteAddressInfo = (id) =>
  requestUser({
    url: "/deleteAddressInfo",
    data: {
      id,
    },
    method: "post",
  });

//保存订单信息
export const reqSavePayInfo = (info) =>
  requestUser({
    url: "/savePayInfo",
    data: {
      info,
    },
    method: "post",
  });

//获取订单信息
export const reqGetPayInfo = (orderNumber) =>
  requestUser({
    url: "/getPayInfo",
    data: {
      orderNumber,
    },
    method: "post",
  });

//存储订单号
export const reqSaveOrderNumber = (orderNumber) =>
  requestUser({
    url: "/saveOrderNumber",
    data: {
      orderNumber,
    },
    method: "post",
  });

//获取订单号
export const reqGetOrderNumber = () =>
  requestUser({
    url: "/getOrderNumber",
    method: "get",
  });

// 获取订单信息
export const mockPayInfo = () => mockAjax.get("/PayInfo");

// 获取支付二维码
export const mockGetCode = (orderId) =>
  request({ url: `/payment/weixin/createNative/${orderId}`, method: "get" });

// 删除商品信息
export const mockDeleteData = (id) => mockAjax.get(`/delete?${id}`);

// 搜索商品信息
export const mockSearchData = (keyWrad) => mockAjax.get(`/search?${keyWrad}`);
