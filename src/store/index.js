import Vue from 'vue'
import Vuex from 'vuex'
import shopCart from './shopCart'
import user from './user'
import Mall from './Mall'
import address from './address'
import payInfo from './payInfo'
Vue.use(Vuex)

export default new Vuex.Store({
    modules:{
        shopCart,
        user,
        Mall,
        address,
        payInfo
    }
})