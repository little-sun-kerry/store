import {
  reqSaveAddressInfo,
  reqGetAddressInfo,
  reqChangeDefaultAddress,
  reqDeleteAddressInfo,
} from "@/api";
export default {
  namespaced: true,
  state: {
    addressInfos: [],
  },
  actions: {
    async saveAddressInfo({ commit }, info) {
      try {
        let result = await reqSaveAddressInfo(info);
        if (result.status == 0) {
          commit("SAVEADDRESSINFOS");
        } else {
          return Promise.reject(new Error("faile"));
        }
      } catch (error) {
        console.log(error.message);
      }
    },
    async getAddressInfo({ commit }) {
      try {
        let result = await reqGetAddressInfo();
        if (result.status == 0) {
          commit("GETADDRESSINFOS", result.data);
        } else {
          return Promise.reject(new Error("faile"));
        }
      } catch (error) {
        console.log(error.message);
      }
    },
    async changeDefaultAddressInfo({ commit }, id) {
      try {
        let result = await reqChangeDefaultAddress(id);
        console.log(result, id);
        if (result.status == 0) {
          commit("CHANGEDEFAULTADDRESSINFOS");
        } else {
          return Promise.reject(new Error("faile"));
        }
      } catch (error) {
        console.log(error.message);
      }
    },
    async deleteAddressInfo({ commit }, id) {
      try {
        let result = await reqDeleteAddressInfo(id);
        if (result.status == 0) {
          commit("DELETEADDRESSINFOS", result.message);
        } else {
          return Promise.reject(new Error("faile"));
        }
      } catch (error) {
        console.log(error.message);
      }
    },
  },
  mutations: {
    SAVEADDRESSINFOS(state) {
      console.log("地址保存成功");
    },
    GETADDRESSINFOS(state, info) {
      console.log(info);
      let arr = {};
      info.forEach((item, index) => {
        if (item.isShow == true) {
          arr = item;
          info.splice(index, 1);
          info.unshift(arr);
          return;
        }
      });
      state.addressInfos = info;
    },
    CHANGEDEFAULTADDRESSINFOS(state) {
      console.log("默认地址修改成功");
    },
    DELETEADDRESSINFOS(state, message) {
      console.log(message);
    },
  },
  getters: {},
};
