import {
  reqSavePayInfo,
  reqGetPayInfo,
  reqSaveOrderNumber,
  reqGetOrderNumber,
} from "@/api";
export default {
  namespaced: true,
  state: {
    shopInfo: [],
    orderNumberArr: [],
  },
  actions: {
    //存储订单信息
    async savePayInfo({ commit }, info) {
      try {
        let result = await reqSavePayInfo(info);
        if (result.status == 0) {
          commit("SAVEPAYINFOS", result.message);
        } else {
          return Promise.reject(new Error("faile"));
        }
      } catch (error) {
        console.log(error.message);
      }
    },
    //存储订单号
    async saveOrderNumber({ commit }, orderNumber) {
      try {
        let result = await reqSaveOrderNumber(orderNumber);
        console.log(result);
        if (result.status == 0) {
          commit("SAVEORDERNUMBERS", result.message);
        } else {
          return Promise.reject(new Error("faile"));
        }
      } catch (error) {
        console.log(error.message);
      }
    },
    //获取订单信息
    async getPayInfo({ commit }, orderNumber) {
      console.log(orderNumber);
      try {
        let result = await reqGetPayInfo(orderNumber);
        if (result.status == 0) {
          commit("GETPAYINFOS", result.data);
        } else {
          return Promise.reject(new Error("faile"));
        }
      } catch (error) {
        console.log(error.message);
      }
    },
    //获取订单号
    async getOrderNumber({ commit }) {
      try {
        let result = await reqGetOrderNumber();
        if (result.status == 0) {
          commit("GETORDERNUMBERS", result.data);
        } else {
          return Promise.reject(new Error("faile"));
        }
      } catch (error) {
        console.log(error.message);
      }
    },
  },
  mutations: {
    SAVEPAYINFOS(state, message) {
      console.log(message);
    },
    GETPAYINFOS(state, data) {
      state.shopInfo = data;
    },
    SAVEORDERNUMBERS(state, message) {
      console.log(message);
    },
    GETORDERNUMBERS(state, data) {
      state.orderNumberArr = data;
    },
  },
  getters: {},
};
