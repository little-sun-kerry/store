import { reqGetCartInfo, reqSaveCartInfo, reqDeleteCartInfo,reqUpdateCartInfo } from "@/api";
export default {
  namespaced: true,
  state: {
    list: [],
  },
  actions: {
    //存储加入购物车的商品
    async SaveCartInfo({ commit }, info) {
      try {
        let result = await reqSaveCartInfo(info);
        console.log(result);
        if (result.status == 0) {
          commit("SAVECARTINFOS", result.message);
        } else {
          return Promise.reject(new Error("faile"));
        }
      } catch (error) {
        console.log(error.message);
      }
    },
    //获取该用户购物车的商品信息
    async getCartInfo({ commit }) {
      try {
        let result = await reqGetCartInfo();
        if (result.status == 0) {
          commit("GETCARTINFOS", result.data);
        } else {
          return Promise.reject(new Error("faile"));
        }
      } catch (error) {
        console.log(error.message);
      }
    },
    //更新该用户购物车商品信息
    async updateCartInfo({commit},info){
      try {
        let result=await reqUpdateCartInfo(info)
        if(result.status==0){
          commit('UPDATECARTINFOS',result.message)
        }else{
          return Promise.reject(new Error("faile"))
        }
      } catch (error) {
        console.log(error.message);
      }
    },
    // 在购物车删除指定的产品
    async deleteCartInfo({ commit }, info) {
      try {
        let result = await reqDeleteCartInfo(info);
        if (result.status == 0) {
          commit("DELETEDATA",result.message);
        }
      } catch (error) {
        console.log(error.message);
      }
    },
  },
  mutations: {
    DELETEDATA(state, message) {
      console.log(message);
    },
    SAVECARTINFOS(state, message) {
      console.log(message);
    },
    GETCARTINFOS(state, data) {
      state.list = data;
    },
    UPDATECARTINFOS(state){
      console.log("更新成功");
    }
  },
  getters: {},
};
