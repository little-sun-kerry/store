import { reqLoginInfo, reqGetUserInfo,reqLogOut } from "@/api";
import router from "@/router";
export default {
  namespaced: true,
  state: {
    userinfo: {},
  },
  actions: {
    async getLoginInfo({ commit }, user) {
      try {
        let result = await reqLoginInfo(user.username, user.password);
        if (result.status == 0) {
          commit("GETlOGINDATA", result);
        } else {
          return Promise.reject(new Error("faile"));
        }
      } catch (error) {}
    },
    async getUserInfo({ commit }) {
      try {
        let result = await reqGetUserInfo();
        if (result.status == 0) {
          commit("GETUSERINFOS", result.data);
        } else {
          return Promise.reject(new Error("faile"));
        }
      } catch (error) {
        console.log(error.message);
      }
    },
    async Logout({commit}){
        try {
           let result=await reqLogOut()
           if(result.status==0){
            commit('LOGOUTS')
           }else{
            return Promise.reject(new Error('faile'))
           }
        } catch (error) {
            console.log(error.message);
        }
    }
  },
  mutations: {
    GETlOGINDATA(state, user) {
      localStorage.setItem("Token", JSON.stringify(user.token));
      localStorage.setItem("userToken", JSON.stringify(user.userToken));
      router.push("/Home");
    },
    LOGOUTS(state) {
      localStorage.removeItem("Token");
      localStorage.removeItem("userToken")
      state.userinfo = "";
    },
    GETUSERINFOS(state, userinfo) {
      state.userinfo = userinfo;
    },
  },
  getters: {},
};
