import { mockShopCartInfo } from "@/api";
export default {
  namespaced: true,
  state: {
    shopInfos: [],
  },
  actions: {
    async getShopInfo({ commit }, keyWrad) {
      try {
        let result = await mockShopCartInfo(keyWrad);
        if (result.status == 200) {
          commit("GETSHOPINFOS", result.shopCartInfo);
        } else {
          return Promise.reject(new Error("faile"));
        }
      } catch (error) {
        console.log(error.message);
      }
    },
  },
  mutations: {
    GETSHOPINFOS(state, data) {
      state.shopInfos = data;
    },
  },
  getters: {},
};
