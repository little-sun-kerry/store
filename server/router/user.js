const express=require('express')
const router=express.Router()

const userHandler=require('../router_handler/user')

const expressJoi=require('@escook/express-joi')
const { user_regUser_schema,user_Login_schema }=require('../schema/user')

router.post('/Login',expressJoi(user_Login_schema),userHandler.Login)

router.post('/regUser',expressJoi(user_regUser_schema),userHandler.RegUser)

router.get('/userinfo',userHandler.UserInfo)

router.get('/Logout',userHandler.Logout)

module.exports=router