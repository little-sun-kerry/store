const express=require('express')
const router=express.Router()

const addressHandler=require('../router_handler/address')

router.post('/saveAddressInfo',addressHandler.Save)

router.get('/getAddressInfo',addressHandler.Get)

router.post('/changeDefaultAddress',addressHandler.ChangeDefaultAddress)

router.post("/deleteAddressInfo",addressHandler.Delete)

module.exports=router