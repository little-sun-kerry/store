const express=require('express')
const router=express.Router()

const orderNumberHandler=require('../router_handler/orderNumber')

router.post('/saveOrderNumber',orderNumberHandler.Save)

router.get('/getOrderNumber',orderNumberHandler.Get)

module.exports=router