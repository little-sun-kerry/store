const express=require('express')
const router=express.Router()

const payInfoHandler=require('../router_handler/payInfo')

router.post('/savePayInfo',payInfoHandler.Save)

router.post('/getPayInfo',payInfoHandler.Get)

module.exports=router