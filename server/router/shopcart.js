const express=require('express')
const router=express.Router()

const shopCartHandler=require('../router_handler/shopcart')

router.post('/SaveCartInfo',shopCartHandler.Save)

router.get('/GetCartInfo',shopCartHandler.Acquire)

router.post('/DeleteCartInfo',shopCartHandler.Delete)

router.post('/UpdateCartInfo',shopCartHandler.Update)

module.exports=router