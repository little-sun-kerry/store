const joi = require("joi");

const username = joi.string().min(2).max(12).required();
const password = joi
  .string()
  .pattern(/^[\S]{6,12}$/)
  .required();
const confirmPassword = joi.ref("password");

exports.user_regUser_schema = {
  body: {
    username,
    password,
    confirmPassword,
  },
};

exports.user_Login_schema = {
  body: {
    username,
    password,
  },
};
