const { request } = require("express");
const db = require("../db");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");

exports.Login = (req, res) => {
  let { username, password } = req.body;
  const sqlStr = "select * from userInfo where username=?";
  db.query(sqlStr, username, (err, results) => {
    if (err) return res.cc(err);
    if (results.length !== 1) return res.cc("登录失败!");
    const validation = bcrypt.compareSync(password, results[0].password);
    if (!validation) return res.cc("密码错误!");
    const user = { ...results[0], password: "", user_pic: "" };
    const tokenStr = jwt.sign(user, config.jwtSecretKey, {
      expiresIn: config.expiresIn,
    });
    res.send({
      status: "0",
      message: "登录成功!",
      token: "Bearer " + tokenStr,
      userToken: tokenStr,
    });
  });
};

exports.RegUser = (req, res) => {
  let { username, password } = req.body;
  password = bcrypt.hashSync(password, 10);
  const sqlStr = "insert into userInfo set ?";
  db.query(sqlStr, { username, password }, (err, results) => {
    if (err) return res.cc(err);
    if (results.affectedRows !== 1) return res.cc("注册失败,请稍后再试!");
    res.send({
      status: 0,
      message: "注册成功！",
    });
  });
};

exports.UserInfo = (req, res) => {
  let data = "";
  if (req.headers.usertoken) {
    data = jwt.verify(req.headers.usertoken, config.jwtSecretKey);
  }
  res.send({
    status: "0",
    message: "获取成功",
    data,
  });
};

exports.Logout=(req,res)=>{
  res.send({
    status:'0',
    message:"退出成功！"
  })
}
