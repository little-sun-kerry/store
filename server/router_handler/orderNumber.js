const db = require("../db");
const jwt = require("jsonwebtoken");
const config = require("../config");

exports.Save = (req, res) => {
  const { orderNumber } = req.body;
  console.log(orderNumber);
  const belong = jwt.verify(
    req.headers.usertoken,
    config.jwtSecretKey
  ).username;
  const sqlStr = "insert into orderNumber_info set ?  ";
  db.query(sqlStr, { orderNumber, belong }, (err, results) => {
    if (err) return res.cc(err);
    if (results.affectedRows !== 1) return res.cc("保存失败");
    res.send({
      status: 0,
      message: "保存成功",
    });
  });
};

exports.Get = (req, res) => {
  let belong = jwt.verify(req.headers.usertoken, config.jwtSecretKey).username;
  const sqlStr = "select * from orderNumber_info where belong=?";
  db.query(sqlStr, belong, (err, results) => {
    if (err) return res.cc(err);
    if (results.length < 1) return res.cc("获取失败");
    let arr=[]
    results.forEach(item=>{
      arr.push(item.ordernumber)
    })
    res.send({
      status: 0,
      message: "获取成功",
      data: arr,
    });
  });
};
