const db = require("../db");
const jwt = require("jsonwebtoken");
const config = require("../config");

exports.Save = (req, res) => {
  let arr = [];
  if (!JSON.parse(req.body.info).length) {
    arr.push(JSON.parse(req.body.info));
  } else {
    arr = JSON.parse(req.body.info);
  }
  console.log(arr);
  const sqlStr = "insert into pay_info set ?";
  arr.forEach((item, index) => {
    db.query(sqlStr, item, (err, results) => {
      if (err) return res.cc(err);
      if (results.affectedRows !== 1) return res.cc("保存失败");
      if (index === arr.length - 1) {
        res.send({
          status: 0,
          message: "保存成功",
        });
      }
    });
  });
};

exports.Get = (req, res) => {
  const orderNumber = JSON.parse(req.body.orderNumber);
  console.log(orderNumber);
  const isTrue = typeof orderNumber;
  const belong = jwt.verify(
    req.headers.usertoken,
    config.jwtSecretKey
  ).username;
  let arr = [];
  const sqlStr = "select * from pay_info where orderNumber=? and belong=?";
  if (isTrue == "number") {
    db.query(sqlStr, [orderNumber, belong], (err, results) => {
      if (err) return res.cc(err);
      if (results.length < 1) return res.cc("获取失败");
      res.send({
        status: 0,
        message: "获取成功",
        data: results,
      });
    });
  }
  if (isTrue == "object") {
    orderNumber.forEach((item, index) => {
      db.query(sqlStr, [item, belong], (err, results) => {
        if (err) return res.cc(err);
        if (results.length < 1) return res.cc("获取失败");
        results.forEach((item) => {
          item.isShow = item.isShow == "1" ? true : false;
        });
        arr.push(results);
        console.log(arr);
        if (index === orderNumber.length - 1) {
          res.send({
            status: 0,
            message: "获取成功",
            data: arr,
          });
        }
      });
    });
  }
};
