const db = require("../db");
const jwt = require("jsonwebtoken");
const config = require("../config");

exports.Save = (req, res) => {
  let info = JSON.parse(req.body.info);
  const userinfo = jwt.verify(req.headers.usertoken, config.jwtSecretKey);
  const belong = userinfo.username;
  info = { ...info, belong };
  const sqlStr = "select * from shopcart_info where id=? and belong=?";
  db.query(sqlStr, [info.id, info.belong], (err, results) => {
    if (err) return res.cc(err);
    if (results.length !== 1) {
      const sqlStr = "insert into shopcart_info set ?";
      db.query(sqlStr, info, (err, results) => {
        if (err) return res.cc(err);
        if (results.affectedRows !== 1) return res.cc("保存失败");
        return res.send({
          status: 0,
          message: "保存成功",
        });
      });
      return 0;
    }
    console.log(info);
    info.skuNum += results[0].skuNum;
    const sqlStr = "update shopcart_info set skuNum=? where id=? and belong=?";
    db.query(sqlStr, [info.skuNum, info.id, info.belong], (err, results) => {
      if (err) return res.cc(err);
      if (results.affectedRows !== 1) return res.cc("更新失败");
      res.send({
        status: 0,
        message: "更新成功",
      });
    });
  });
};

exports.Acquire = (req, res) => {
  const info = jwt.verify(req.headers.usertoken, config.jwtSecretKey).username;
  const sqlStr = "select * from shopcart_info where belong=?";
  db.query(sqlStr, [info], (err, results) => {
    if (err) return res.cc(err);
    if (results.length < 0) return res.cc("获取失败");
    results.forEach((item) => {
      item.isShow = item.isShow == 1 ? true : false;
    });
    res.send({
      status: "0",
      message: "获取成功",
      data: results,
    });
  });
};

exports.Update = (req, res) => {
  let arr = [];
  arr = JSON.parse(req.body.info);
  const sqlStr = "update shopcart_info set ? where id=? and belong=?";
  arr.forEach((item, index) => {
    item.isShow = String(item.isShow);
    db.query(sqlStr, [item, item.id, item.belong], (err, results) => {
      if (err) return res.cc(err);
      if (results.affectedRows !== 1) return res.cc("更新失败");
      if (index === arr.length - 1) {
        res.send({
          status: 0,
          message: "更新成功",
        });
      }
    });
  });
};

exports.Delete = (req, res) => {
  let arr = [];
  if (!JSON.parse(req.body.info).length) {
    arr.push(JSON.parse(req.body.info));
  } else {
    arr=JSON.parse(req.body.info);
  }
  const sqlStr = "delete from shopcart_info where id=? and belong=?";
  arr.forEach((item, index) => {
    db.query(sqlStr, [item.id, item.belong], (err, results) => {
      // console.log(results);
      if (err) return res.cc(err);
      if (results.affectedRows !== 1) return res.cc("删除失败");
      if (index === arr.length - 1) {
        res.send({
          status: 0,
          message: "删除成功",
        });
      }
    });
  });
};
