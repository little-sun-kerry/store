const db = require("../db");

exports.Save = (req, res) => {
  let info = JSON.parse(req.body.info);
  if (!info.id) {
    console.log(info);
    const sqlStr = "insert into addressinfo set ?";
    db.query(sqlStr, info, (err, results) => {
      if (err) return res.cc(err);
      if (results.affectedRows !== 1) return res.cc("保存失败");
      return res.send({
        status: 0,
        message: "保存成功",
      });
    });
    return 0;
  }
  const sqlStr = "select * from addressinfo where id=?";
  db.query(sqlStr, info.id, (err, results) => {
    if (err) return res.cc(err);
    if (results.length !== 1) return res.cc("查询失败");
    const sqlStr = "update addressInfo set ?";
    db.query(sqlStr, info, (err, results) => {
      if (err) return res.cc(err);
      if (results.affectedRows !== 1) return res.cc("更新成功");
      res.send({
        status: 0,
        message: "修改成功",
      });
    });
  });
};

exports.ChangeDefaultAddress = (req, res) => {
  const id = req.body.id;
  const sqlStr = "select * from addressinfo";
  db.query(sqlStr, (err, results) => {
    if (err) return res.cc(err);
    if (results.length < 1) return res.cc("查询失败");
    results.forEach((item) => {
      item.isShow = false;
      if (item.id == id) item.isShow = true;
    });
    results.forEach((item, index, arr) => {
      const sqlStr = "select * from addressinfo where id=?";
      db.query(sqlStr, [item.id], (err, results) => {
        if (err) return res.cc(err);
        if (results.length < 1) return res.cc("该id不存在");
        const sqlStr = "update addressinfo set isShow=? where id=?";
        db.query(sqlStr, [item.isShow, results[0].id], (err, results) => {
          if (err) return res.cc(err);
          if (results.affectedRows !== 1) return res.cc("修改失败");
          if (arr.length - 1 == index) {
            res.send({
              status: 0,
              message: "修改成功",
            });
          }
        });
      });
    });
  });
};

exports.Get = (req, res) => {
  const sqlStr = "select * from addressinfo";
  db.query(sqlStr, "", (err, results) => {
    if (err) return res.cc(err);
    if (results.length < 1) return res.cc("查找失败");
    results.forEach((item) => {
      item.isShow = item.isShow == 1 ? true : false;
    });
    console.log(results.length);
    res.send({
      status: 0,
      message: "获取成功",
      data: results,
    });
  });
};

exports.Delete = (req, res) => {
  let { id } = req.body;
  const sqlStr = "delete from addressinfo where id=?";
  db.query(sqlStr, id, (err, results) => {
    if (err) return res.cc(err);
    if (results.affectedRows !== 1) return res.cc("删除失败");
    res.send({
      status: 0,
      message: "地址删除成功",
    });
  });
};
