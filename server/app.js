const express = require("express");
const app = express();

const joi = require("joi");

const expressJwt = require("express-jwt");
const config = require("./config");

const cors = require("cors");
app.use(cors());

app.use(express.urlencoded({ extended: false }));

app.use((req, res, next) => {
  res.cc = function (err, status = 1) {
    res.send({
      status,
      message: err instanceof Error ? err.message : err,
    });
  };
  next();
});

app.use(
  expressJwt({ secret: config.jwtSecretKey }).unless({
    path: [/\/Login/, /\/regUser/, /\/userinfo/],
  })
);

// 用户
const routerUser = require("./router/user");
app.use("/user", routerUser);

//购物车
const routerShopCart = require("./router/shopcart");
app.use("/user", routerShopCart);

//用户地址信息
const routerAddress = require("./router/address");
app.use("/user", routerAddress);

//用户订单信息
const routerPayInfo = require("./router/payInfo");
app.use("/user", routerPayInfo);

//用户订单号
const routerOrderNumber = require("./router/orderNumber");
app.use("/user", routerOrderNumber);

app.use((err, req, res, next) => {
  if (err instanceof joi.ValidationError) return res.cc(err);
  if (err.name === "UnauthorizedError") return res.cc("身份认证失败！");
  res.cc(err);
});

app.listen(8000, () => {
  console.log("api_server服务已经启动：http://127.0.0.1:8000");
});
